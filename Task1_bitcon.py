import json
import requests
import sqlite3

from flask import Flask, request, make_response
from faker import Faker
from db import execute_query

apo = Flask(__name__)


@apo.route('/bitcoin_rate')
def get_bitcoin_rate():
    d = requests.get("https://bitpay.com/api/rates")
    data = d.json()
    currency = request.args.get('сurrency', 'USD')
    for item in data:
        if item['code'] == currency:
            return (f"{item['code']} - {item['rate']}")


@apo.route('/random_users')
def gen_random_users():
    fake = Faker()
    count = int(request.args.get('count', '10'))
    users_list = []
    for i in range(count):
        users_list.append(f'{fake.email()} | {fake.name()}')

    response = make_response("\n".join(users_list), count)
    response.headers['Content-Type'] = 'text/plain'
    return response


@apo.route('/unique_names')
def get_unique_names():
    result = execute_query("SELECT DISTINCT FirstName FROM customers")
    countfname = len(result)
    return countfname

@apo.route('/tracks_count')
def get_tracks_count():
    result = execute_query("SELECT * FROM tracks")
    countline = len(result)
    return countline

@apo.route('/customers')
def get_customers():
    city = request.args['city']
    country = request.args['country']

    result = execute_query("""SELECT * from Customers
        WHERE City = ? AND Country = ?""", (city, country, ))

    result_list = []
    for item in result:
        result_list.append(str(item))
    response = make_response("\n".join(result_list))
    response.headers['Content-Type'] = 'text/plain'
    return response


apo.run(debug=True)